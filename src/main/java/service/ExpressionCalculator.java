package service;

/**
 *  this class checks for patterns in a string and modifies the equation stack based on pattern matches
 */
public class ExpressionCalculator {

    /**
     * @param s a string which represents an expression such as sqrt(46.245)
     * @return the string value of the square root operation applied on the sqrt parameter
     */
    private static String exprIsSqrt(String s) {
        String[] sqrt = s.split("sqrt\\(|\\)");
        double x = Double.parseDouble(sqrt[1]);
        x = Math.sqrt(x);
        return String.valueOf(x);
    }

    /**
     * @param s a string which represents an expression such as max(1,2)
     * @return the string value of the greater parameter of the max function
     */
    private static String exprIsMax(String s) {
        String[] max = s.split("max\\(|\\)|,");
        double x = Math.max(Double.parseDouble(max[1]),Double.parseDouble(max[2]));
        return String.valueOf(x);
    }

    /**
     * @param s a string which represents an expression such as min(1,2)
     * @return the string value of the lesser parameter of the min function
     */
    private static String exprIsMin(String s) {
        String[] min = s.split("min\\(|\\)|,");
        double x = Math.min(Double.parseDouble(min[1]),Double.parseDouble(min[2]));
        return String.valueOf(x);
    }

    /**
     * @param s a string representing a token extracted from the original equation
     * @return the string result of the operation or the number itself
     * @throws Exception if the pattern of the token is not one of the 4 known patterns:
     * number, sqrt(number), min(number, number), max(number, number)
     */
    public static String parseOperand(String s) throws Exception {
        final String DOUBLE = "(0|[1-9][0-9]*)(\\.[0-9]+)?";
        if (s.matches("sqrt\\("+DOUBLE+"\\)")) {
            return exprIsSqrt(s);
        }
        else if (s.matches("max\\(" + DOUBLE +"," + DOUBLE +"\\)")) {
            return exprIsMax(s);
        }
        else if (s.matches("min\\(" + DOUBLE +"," + DOUBLE +"\\)")) {
            return exprIsMin(s);
        }
        else if (s.matches(DOUBLE)) {
            return s;
        }
        else {
            throw new Exception("UNKNOWN PATTERN: " + s);
        }
    }

    /**
     * @param s an operator as a string ( +, -, /, *)
     * @param first the first term of the operation
     * @param second the second term of the operation
     * @return the string value of the result of the operation
     * @throws Exception if the operator is not one of the 4 known operators
     */
    public static String calculateExpression(String s, String first, String second) throws Exception {
        double first_term = Double.parseDouble(first);
        double second_term = Double.parseDouble(second);
        switch (s) {
            case "+" -> { return String.valueOf(first_term + second_term); }
            case "-" -> { return String.valueOf(first_term - second_term); }
            case "*" -> { return String.valueOf(first_term * second_term); }
            case "/" -> {
                if (second_term==0) throw new Exception("DIVISION BY ZERO");
                return String.valueOf(first_term / second_term);
            }
            default -> throw new Exception("UNKNOWN OPERATOR");
        }
    }
}
