package service;

import java.util.List;
import java.util.Stack;


/**
 * this singleton class takes as a parameter an equation as a string and returns the result of that equation
 */
public class EquationCalculator {
    /**
     *  the only instance of this class - lazy initialization
     */
    private static EquationCalculator equationParser = null;
    /**
     *  private constructor
     */
    private EquationCalculator() {}
    /**
     * @return the only instance of this class and if not initiated do that as well
     */
    public static EquationCalculator getInstance() {
        if (equationParser==null) {
            equationParser = new EquationCalculator();
        }
        return equationParser;
    }

    /**
     * @param equation an equation as a string which can deal with +,-,/,*, min(term1, term2),
     *                 max(term1, term2), sqrt(x)
     * @return the result of the given equation as a double
     * @throws Exception if the equation is not written correctly, division by zero or an operator is invalid
     */
    public double calculate(String equation) throws Exception {
        List<String> parsed = EquationParser.parseEquation(equation);
        Stack<String> stack = new Stack<>();
        for (String s : parsed) {
            String tempRes;
            if (s.matches("^[-+/*]$")) {
                String second = stack.pop();
                String first = stack.pop();
                tempRes = ExpressionCalculator.calculateExpression(s, first, second);
            }
            else {
                tempRes = ExpressionCalculator.parseOperand(s);
            }
            stack.push(tempRes);
        }
        return Double.parseDouble(stack.pop());
    }

}
