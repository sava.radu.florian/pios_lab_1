package service;

import java.util.*;

/**
 * this class can split an equation string into tokens and return the newly created list in postfix form
 */
public class EquationParser {

    /**
     * @param s the operator whose priority is decided
     * @return 1 - (for + and - );
     *         2 - (for * and / );
     *         0 - (for anything else)
     */
    private static int priority(String s) {
        return
                switch (s) {
                    case "+", "-" -> 1;
                    case "*", "/" -> 2;
                    default -> 0;
                };
    }

    /**
     * @param equation as a string
     * @return a list containing the tokens (no whitespaces) in the order in which they appear
     *         NOTE: a token is either an operator(+,-,/,*), a number (e.q.: 2, 3.045 ...) or
     *               a construct such as max(1,2), min(0.47, 5), sqrt(0.49)
     */
    private static List<String> splitEquation(String equation) {
        return List.of(equation.trim().replace(" ", "").split("((?<=[-+/*])|(?=[-+/*]))"));
    }

    /**
     * @param equation to be passed as a string
     * @return a list of tokens explained above in postfix form
     */
    public static List<String> parseEquation(String equation) {
        List<String> parsed = splitEquation(equation);
        parsed = convertToPostFixed(parsed);
        return parsed;
    }

    /**
     * @param exp a list of tokens extracted from an expression
     * @return another list of the same tokens but in postfix form, making it easier for later processing
     *
     *  NOTE: at each operator verification checkpoint we also check for priority and take decisions accordingly
     */
    private static List<String> convertToPostFixed(List<String> exp) {
        List<String> exprs = new ArrayList<>();
        Stack<String> stack = new Stack<>();

        for (String s : exp) {
            if (s.matches("^[-+/*]$")) {
                while (!stack.isEmpty() && priority(s) <= priority(stack.peek())) {
                    exprs.add(stack.peek());
                    stack.pop();
                }
                stack.push(s);
            } else
                exprs.add(s);
        }

        while (!stack.isEmpty()) {
            exprs.add(stack.peek());
            stack.pop();
        }
        return exprs;
    }
}
