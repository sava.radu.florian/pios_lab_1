package main;

import service.EquationCalculator;
import java.util.Scanner;


/**
 *  the main class which runs our project
 */
public class Main {
    /**
     * @param args nothing, we are going to read using a scanner
     * options: exit - will exit the program ;
     *          equation - will calculate the result (e.g. 1 + 2 * 3 - 4 /5 + max(0.8, 0) + min(2, 4) - sqrt(4))
     */
    public static void main(String[] args) {
        while(true) {
            Scanner scanner = new Scanner(System.in);
            System.out.print(" Insert your equation: ");
            String equation = scanner.nextLine();
            if (equation.equals("exit"))
                break;

            EquationCalculator equationCalculator = EquationCalculator.getInstance();

            try {
                System.out.println(" !!!RESULT!!! " + equationCalculator.calculate(equation) + '\n');
            } catch (Exception ex) {
                System.out.println(" !!!ERROR!!! " + ex.getMessage());
            }
        }
    }
}
