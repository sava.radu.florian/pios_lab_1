package service;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

class EquationCalculatorTest {
    private final EquationCalculator eqc = EquationCalculator.getInstance();

    @Test
    void whenCalculatingCorrectEquationThenItShouldReturnItsResultAsADouble() {
        try {
            double EPSILON = 0.00001;
            assertTrue(EPSILON > -4 - eqc.calculate("5+max(4,5)-sqrt(4)-4*3"));
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            fail();
        }

    }
}