package service;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


public class ExpressionCalculatorTest {
    private final double ACCEPTED_ERROR = 0.00001;

    @Test
    void whenCalculatingExpressionAddWithTwoDoublesThenItShouldReturnTheirSumAsString() {
        try {
            assertTrue(ACCEPTED_ERROR > 5647.8188 - Double.parseDouble(ExpressionCalculator.calculateExpression("+", "5124.1245", "523.6943")) );
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    void whenCalculatingExpressionAddWithTwoDoublesWithOneZeroThenItShouldReturnTheNonZeroNumberAsString() {
        try {
            assertTrue(ACCEPTED_ERROR > 5647.8188 - Double.parseDouble(ExpressionCalculator.calculateExpression("+", "5647.8188", "0.0")) );
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    void whenCalculatingExpressionSubtractWithTwoDoublesWithTheFirstSmallerThanTheSecondThenItShouldReturnTheirDiffAsStringWithNegativeSignInFront() {
        try {
            assertTrue(ACCEPTED_ERROR > -25.535 - Double.parseDouble(ExpressionCalculator.calculateExpression("-", "134.415", "159.95")) );
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    void whenCalculatingExpressionAddWithTwoDoublesWithTheFirstLargerThanTheSecondThenItShouldReturnTheirSumAsString() {
        try {
            assertTrue(ACCEPTED_ERROR > 4.58545 - Double.parseDouble(ExpressionCalculator.calculateExpression("-", "14", "9.41455")) );
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    void whenCalculatingExpressionMultiplyWithTwoDoublesThenItShouldReturnTheirMultiplicationAsString() {
        try {
            assertTrue(ACCEPTED_ERROR > 287831.326356 - Double.parseDouble(ExpressionCalculator.calculateExpression("*", "581.414", "495.054")) );
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    void whenCalculatingExpressionMultiplyWithTwoDoublesAndOneZeroThenItShouldReturnZeroAsString() {
        try {
            assertTrue(ACCEPTED_ERROR > 0 - Double.parseDouble(ExpressionCalculator.calculateExpression("*", "581.414", "0")) );
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    void whenCalculatingExpressionDivideWithTwoDoublesThenItShouldReturnTheirDivisionAsString() {
        try {
            assertTrue(ACCEPTED_ERROR > 2 - Double.parseDouble(ExpressionCalculator.calculateExpression("/", "59734156", "29867078")) );
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    void whenCalculatingExpressionDivideWithTwoDoublesAndOneZeroThenItShouldThrowAnException() {
        try {
            ExpressionCalculator.calculateExpression("/", "5", "0");
            fail();
        }
        catch(Exception ex) {
            assertEquals("DIVISION BY ZERO" , ex.getMessage());
        }
    }

    @Test
    void whenCalculatingExpressionDivideWithTwoDoublesAndOneLessThanOneThenItShouldReturnTheirDivisionAsString() {
        try {
            assertTrue(ACCEPTED_ERROR > 18 - Double.parseDouble(ExpressionCalculator.calculateExpression("/", "4.5", "0.25")) );
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    void whenCalculatingExpressionWithTwoCharacterStringsThenItShouldThrowException() {
        try {
            ExpressionCalculator.calculateExpression("+", "514b", "0.51x3");
            fail();
        }
        catch(Exception ex) {
            assertEquals(NumberFormatException.class , ex.getClass());
        }
    }

    @Test
    void whenCalculatingExpressionWithTwoNullStringsThenItShouldThrowException() {
        try {
            ExpressionCalculator.calculateExpression("+", "555", "");
            fail();
        }
        catch(Exception ex) {
            assertEquals(NumberFormatException.class , ex.getClass());
        }
    }

    @Test
    void whenCalculatingExpressionWithUndefinedSignThenItShouldThrowException() {
        try {
            ExpressionCalculator.calculateExpression("crazyloop", "420", "420");
            fail();
        }
        catch(Exception ex) {
            assertEquals("UNKNOWN OPERATOR" , ex.getMessage());
        }
    }

    @Test
    void whenParsingNumberOperandWithCorrectNumberThenItShouldReturnItAsAString() {
        try {
            assertEquals("5051.4150", ExpressionCalculator.parseOperand("5051.4150"));
        } catch (Exception ex) {
            fail();
        }
    }

    @Test
    void whenParsingNumberOperandWithIncorrectNumberThenItShouldThrowException() {
        try {
            ExpressionCalculator.parseOperand("crazyloop");
            fail();
        }
        catch(Exception ex) {
            assertEquals("UNKNOWN PATTERN: crazyloop" , ex.getMessage());
        }
    }

    @Test
    void whenParsingMinOperandWithTwoNumbersThenItShouldReturnTheSmallerOneAsAString() {
        try {
            assertEquals("0.5", ExpressionCalculator.parseOperand("min(0.55,0.50)"));
        } catch (Exception ex) {
            fail();
        }
    }

    @Test
    void whenParsingMaxOperandWithTwoNumbersThenItShouldReturnTheBiggerOneAsAString() {
        try {
            assertEquals("0.55", ExpressionCalculator.parseOperand("max(0.55,0.50)"));
        } catch (Exception ex) {
            fail();
        }
    }

    @Test
    void whenParsingSqrtOperandWithOneNumberThenItShouldReturnItsSquareRootAsAString(){
        try {
            assertEquals("2.0", ExpressionCalculator.parseOperand("sqrt(4)"));
        } catch (Exception ex) {
            fail();
        }
    }


}
