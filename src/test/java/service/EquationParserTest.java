package service;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class EquationParserTest {
    @Test
    void whenParsingAnEquationToPostFixedThenItShouldReturnAListOfStrings() {
        assertEquals("[5, max(4,5), +, sqrt(4), -, 4, 3, *, -]", String.valueOf(EquationParser.parseEquation("5+max(4,5)-sqrt(4)-4*3")));
    }

    @Test
    void whenParsingAnEquationToPostFixedThenItShouldReturnAListOfStrings2() {
        assertEquals("[1, 2, +, 3, +, 4, +, 5, +, 15, 0, *, -]", String.valueOf(EquationParser.parseEquation("1+2+3+4+5-15*0")));
    }

    @Test
    void whenParsingAnEquationToPostFixedThenItShouldReturnAListOfStrings3() {
        assertEquals("[1, 2, +, 3, +, 4, +, 5, +, 6, +, 7, +, 8, +, 9, +, 10, +]", String.valueOf(EquationParser.parseEquation("1 + 2 + 3 + 4 + 5 + 6 + 7 + 8 + 9 + 10")));
    }
}
